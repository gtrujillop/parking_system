class LocationParkRecordSerializer < ActiveModel::Serializer
  attributes :id, :vehicle_id, :slot_id, :entry_date, :exit_date, :comments, :total
end
