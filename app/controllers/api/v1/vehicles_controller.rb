module Api::V1
  class VehiclesController < BaseController
    def index
      @user = current_user
      @vehicles = @user.vehicles
      unless @vehicles.empty?
        render json: @vehicles
      else
        render json: {}, status: :not_found
      end
    end

    def new
      @user = current_user
      @vehicle = Vehicle.new
      render json: @vehicle
    end

    def show
      @user = current_user
      @vehicle = Vehicle.by_id(params[:id]).by_user(@user.id).first
      if @vehicle
        render json: @vehicle
      else
        render json: {}, status: :not_found
      end
    end

    def create
      @vehicle = Vehicle.new(vehicle_params)
      @vehicle.user_id = current_user.id
      if @vehicle.save
        render json: @vehicle
      else
        render json: @vehicle.errors, status: :unprocessable_entity
      end
    end

    def edit
      @vehicle = Vehicle.by_id(params[:id]).by_user(current_user.id).first
      if @vehicle
        render json: @vehicle
      else
        render json: @vehicle, status: :not_found
      end
    end

    def update
      @vehicle = Vehicle.by_id(params[:id]).by_user(current_user.id).first
      if @vehicle.update_attributes(vehicle_params)
        render json: @vehicle
      else
        render json: @vehicle.errors, status: :unprocessable_entity
      end
    end

    def destroy
      @vehicle = Vehicle.by_id(params[:id]).by_user(current_user.id).first
      if @vehicle.destroy
        render json: {}, status: :ok
      else
        render json: @vehicle.errors, status: :unprocessable_entity
      end
    end

    def vehicle_params
      params.require(:vehicle).permit(:user_id, :model, :year, :vin, :id)
    end
    private :vehicle_params
  end
end
