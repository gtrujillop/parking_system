module Api::V1
  class UsersController < BaseController
    def show
      @user = current_user
    end
  end
end
