module Api::V1
  class SlotsController < BaseController
    def index
      @location = Location.find(params[:location_id])
      @slot_presenter = SlotPresenter.new(@location)
      @slot_presenter.build_slot_rows
      render json: @slot_presenter.slots, each_serializer: SlotSerializer
    end

    def new
      @location = Location.find(params[:location_id])
      @slot = Slot.new
      render json: @slot
    end

    def create
      @location = Location.find(params[:location_id])
      @created_slots = []
      begin
        Slot.transaction do
          @slots_to_create = params[:slots].to_i
          @slots_to_create.times do
            @slot = Slot.new(is_occupied: 0, location_id: @location.id)
            if @slot.save && @location.update_attributes(max_slots: @location.max_slots - 1)
              @created_slots << @slot
            else
              raise ActiveRecord::Rollback, "Slot could not be created."
            end
          end
          render json: @created_slots
        end
      rescue
        render json: 'Slots could not be created', status: :unprocessable_entity
      end
    end

    def edit
      @location = Location.find(params[:location_id])
      @slot = Slot.by_id(params[:id]).by_location(@location.id).first
      render json: @slot
    end

    def update
      @location = Location.find(params[:location_id])
      @slot = Slot.by_id(params[:id]).by_location(@location.id).first
      if @slot.update_attributes(slot_params)
        render json: @slot
      else
        render json: @slot.errors.full_messages.join(','), status: :unprocessable_entity
      end
    end

    def destroy
      @location = Location.find(params[:location_id])
      @slot = Slot.by_id(params[:id]).by_location(@location.id).first
      if @slot.destroy
        flash[:success] = 'Slot deleted successfully.'
        redirect_to location_slots_path(@location)
      else
        flash[:error] = @slot.errors.full_messages.join(',')
        render 'index'
      end
    end

    def slot_params
      params.require(:slot).permit(:location_id, :id)
    end
    private :slot_params
  end
end
