class UsersController < ApplicationController
  def show
    @user = current_user
  end

  def index
    @users = User.all
    unless @users.present?
      flash[:alert] = 'No users registered.'
    end
  end
end
